﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Uiscore : MonoBehaviour
{

    [Header("UI")]
    public Text scor;
    public Text highScore;

    [Header("Score")]
    public ScoreController scoreController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scor.text = scoreController.GetCurrentScore().ToString();
        highScore.text = score.highScore.ToString();
    }
}
