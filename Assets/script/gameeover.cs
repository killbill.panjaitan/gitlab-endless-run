﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameeover : MonoBehaviour
{
    // Start is called before the first frame update

    public Button retry, exit;

    private void Start()
    {
        retry.onClick.AddListener(ulang);
        exit.onClick.AddListener(keluar);

    }

    public void ulang()
    {

            // reload
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void keluar()
    {

        Application.Quit();

    }
}
